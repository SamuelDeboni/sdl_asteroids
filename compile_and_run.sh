#!/bin/sh
cd ~/projects/hys_engine
flags="-lSDL2 -lSDL2main -lSDL2_image -lSDL2_ttf -lm"
compiler="clang"

output="hydengine"
input="code/hydengine.cpp"

rm $output

echo "Compiling with " $compiler
time $compiler $1 $input -o $output $flags

exec "./$output"
