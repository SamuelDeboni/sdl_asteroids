#include "hydengine.hpp"

struct Entity
{
    HSE_Sprite sprite;
    vector2 pos;
    vector2 vel;
    i32 id;
    bool32 dead;
};

struct Entities
{
    Entity* a;
    int size;
};

void InitEntities(Entities* ent, int size)
{
    ent->a = (Entity*)malloc(size * sizeof(Entity));
    ent->size = size;

    for (int i = 0; i < size; i++)
    {
        ent->a[i].id = -1;
        ent->a[i] = {};
        ent->a[i].sprite = {};
    }
}

