#include "hydengine.hpp"
#include "entity.cpp"

inline bool32
checkCircleCollision(f32 x1, f32 x2, f32 y1, f32 y2, f32 radius) {
    return (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) < (radius+16)*(radius+16);
}

// Global variables
Entity *ast;
Entity ship = {};
Entity *shot;

internal void
quit()
{
    // Deinitialize renderer
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    
    TTF_Quit();
    IMG_Quit();
    SDL_Quit();
}

internal SDL_Texture*
HyS_load_texture(const char* fn)
{
    SDL_Texture* tmp;
    SDL_Surface* surface = IMG_Load(fn);
    
    if (surface) 
    {
        tmp = SDL_CreateTextureFromSurface (renderer, surface);
        if (!tmp)
            tmp = potato_texture;
    }
    else
        tmp = potato_texture;
    
    SDL_FreeSurface (surface);
    return tmp;
}

internal i32 HSE_render_entity(Entity* entity, SDL_Renderer* renderer)
{
    entity->sprite.rect.x = (i32)(entity->pos.x - entity->sprite.center.x);
    entity->sprite.rect.y = (i32)(entity->pos.y - entity->sprite.center.y);
    i32 result = SDL_RenderCopyEx(renderer, entity->sprite.texture, 
                                  &(entity->sprite.crop), &(entity->sprite.rect), 
                                  entity->sprite.angle, &(entity->sprite.center), 
                                  entity->sprite.flip);
    return result;
}

internal void
init()
{
    
    /* Initialize SDL */
    SDL_Init(SDL_INIT_EVERYTHING);
    IMG_Init(IMG_INIT_PNG);
    TTF_Init();
    SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "2" );
    
    /* Create Main window */
    window = SDL_CreateWindow("HyS_Engine",
                              SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED,
                              WINDOW_WIDTH, WINDOW_HEIGHT,
                              0);
    /* Create main renderer*/
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    
    {// Load Global Potato textures
        SDL_Surface* potato_png = IMG_Load ("assets/potato.png");
        potato_texture = SDL_CreateTextureFromSurface(renderer, potato_png);
        potato_texture2 = potato_texture;
        SDL_FreeSurface(potato_png);
    }
    
    // ===================
    // Load Game resources
    // ===================
    
    SDL_Texture* ast1_texture = HyS_load_texture ("assets/ast1.png");
    SDL_Texture* ast2_texture = HyS_load_texture ("assets/ast2.png");
    SDL_Texture* ast3_texture = HyS_load_texture ("assets/ast3.png");
    SDL_Texture* shot_texture = HyS_load_texture ("assets/shot.png");
    
    font = TTF_OpenFont("assets/Px437_IBM_3270pc.ttf",25);
    
    // ===================
    // Create entities
    // ===================
    
    // Init The player
    ship.sprite.texture = HyS_load_texture("assets/Ship.png");
    
    //-------------------------------------------------
    
    shot = (Entity*)calloc(5,sizeof(Entity));
    
    for (int i = 0; i < 5; i++)
    {
        shot[i].sprite.texture = shot_texture;
        shot[i].sprite.rect = {0,0,16,16};
        shot[i].sprite.crop = {0,0,32,32};
        shot[i].sprite.center = {8,8};
        shot[i].sprite.angle = 0;
        shot[i].pos = {0,0};
        shot[i].vel = {0,0};
        shot[i].id = 50+i;
        shot[i].dead = true;
        shot[i].sprite.flip = SDL_FLIP_NONE;
    }
    
    ship.sprite.rect.x = WINDOW_WIDTH/2;
    ship.sprite.rect.y = WINDOW_HEIGHT/2;
    ship.sprite.rect.h = 64;
    ship.sprite.rect.w = 32;
    
    ship.sprite.crop.h = 64;
    ship.sprite.crop.w = 32;
    ship.sprite.crop.x = 0;
    ship.sprite.crop.y = 0;
    
    ship.sprite.angle = 0;
    ship.sprite.flip = SDL_FLIP_NONE;
    ship.sprite.center.x = ship.sprite.center.y = 16;
    ship.pos.x = (float)ship.sprite.rect.x;
    ship.pos.y = (float)ship.sprite.rect.y;
    
    //----------------------------------------------
    ast = (Entity*)calloc(39,sizeof(Entity));
    
    for (int i = 0; i < 3; i++)
    {
        ast[i].id = i;
        ast[i].dead = false;
        ast[i].sprite.rect.h = 128;
        ast[i].sprite.rect.w = 128;
        ast[i].sprite.crop.h = 128;
        ast[i].sprite.crop.w = 128;
        ast[i].sprite.texture = ast3_texture;
        ast[i].sprite.center = {64,64};
        
        ast[i].pos.x = (WINDOW_HEIGHT/3)*cos(i*(float)(120*3.14/180.0)+0.33) + WINDOW_WIDTH/2;
        ast[i].pos.y = (WINDOW_HEIGHT/3)*sin(i*(float)(120*3.14/180.0)+0.33) + WINDOW_HEIGHT/2;
        ast[i].vel.x = (ast[i].pos.x - WINDOW_WIDTH/2)/4;
        ast[i].vel.y = (ast[i].pos.y - WINDOW_HEIGHT/2)/4;
    }
    
    for (int i = 3; i < 12; i++)
    {
        ast[i].id = i;
        ast[i].dead = true;
        ast[i].sprite.rect.h = 64;
        ast[i].sprite.rect.w = 64;
        ast[i].sprite.crop.h = 64;
        ast[i].sprite.crop.w = 64;
        ast[i].sprite.texture = ast2_texture;
        ast[i].sprite.center = {32,32};
        
        ast[i].pos.x = (WINDOW_HEIGHT/4)*cos(i*(float)(120*3.14/180.0)+0.33) + WINDOW_WIDTH/2;
        ast[i].pos.y = (WINDOW_HEIGHT/4)*sin(i*(float)(120*3.14/180.0)+0.33) + WINDOW_HEIGHT/2;
        ast[i].vel.x = (ast[i].pos.x - WINDOW_WIDTH/2)/2;
        ast[i].vel.y = (ast[i].pos.y - WINDOW_HEIGHT/2)/2;
    }
    
    for (int i = 12; i < 39; i++)
    {
        ast[i].id = i;
        ast[i].dead = true;
        ast[i].sprite.rect.h = 32;
        ast[i].sprite.rect.w = 32;
        ast[i].sprite.crop.h = 32;
        ast[i].sprite.crop.w = 32;
        ast[i].sprite.texture = ast1_texture;
        ast[i].sprite.center = {16,16};
        
        ast[i].pos.x = (WINDOW_HEIGHT/4)*cos(i*(float)(120*3.14/180.0)+0.33) + WINDOW_WIDTH/2;
        ast[i].pos.y = (WINDOW_HEIGHT/4)*sin(i*(float)(120*3.14/180.0)+0.33) + WINDOW_HEIGHT/2;
        ast[i].vel.x = (ast[i].pos.x - WINDOW_WIDTH/2);
        ast[i].vel.y = (ast[i].pos.y - WINDOW_HEIGHT/2);
    }
    
}

internal void
resetLevel()
{
    for (int i = 0; i < 5; i++)
    {
        shot[i].pos = {0,0};
        shot[i].vel = {0,0};
        shot[i].dead = true;
    }
    
    ship.sprite.angle = 0;
    ship.vel = {0, 0};
    ship.pos.x = WINDOW_WIDTH/2;
    ship.pos.y = WINDOW_HEIGHT/2;
    
    for (int i = 0; i < 3; i++)
    {
        ast[i].dead = false;
        ast[i].pos.x = (WINDOW_HEIGHT/3)*cos(i*(float)(120*3.14/180.0)+0.33) + WINDOW_WIDTH/2;
        ast[i].pos.y = (WINDOW_HEIGHT/3)*sin(i*(float)(120*3.14/180.0)+0.33) + WINDOW_HEIGHT/2;
    }
    for (int i = 3; i < 39; i++)
        ast[i].dead = true;
}

global f32 shotTimer = 0;
global f32 deathTimer = 1;
global u32 shotCount = 0;
global i32 astCount = 3;
global i32 lives = 3;
global u32 score = 0;
global u32 scoreMult = 10;

internal void
updateAndRenderer(f32 delta, u32 keys)
{
    
    shotTimer += delta;
    deathTimer += delta;
    // === Game Logic  ===
    // Ship movement
    if ((keys & 0x01) == 0x01) ship.sprite.angle += delta * 200;
    if ((keys & 0x02) == 0x02) ship.sprite.angle -= delta * 200;
    if ((keys & 0x04) == 0x04) 
    {
        ship.vel.x += 200*cos(3.14*(ship.sprite.angle-90)/180)*delta;
        ship.vel.y += 200*sin(3.14*(ship.sprite.angle-90)/180)*delta;
        ship.sprite.crop.x = 32;
    }
    else
        ship.sprite.crop.x = 0;
    
    ship.pos.x += ship.vel.x * delta;
    ship.pos.y += ship.vel.y * delta;
    
    // Wrap ship around the screen
    if (ship.pos.y < 0) ship.pos.y = WINDOW_HEIGHT - 1;
    else if (ship.pos.y > WINDOW_HEIGHT) ship.pos.y = 1;
    if (ship.pos.x < 0) ship.pos.x = WINDOW_WIDTH - 1;
    else if (ship.pos.x > WINDOW_WIDTH) ship.pos.x = 1;
    
    if ((keys & 0x8) == 0x08 && shotTimer > 0.5)
    {
        shotTimer = 0;
        shot[shotCount].dead = false;
        
        shot[shotCount].pos.x = ship.pos.x;
        shot[shotCount].pos.y = ship.pos.y;
        
        shot[shotCount].sprite.angle = ship.sprite.angle;
        
        shot[shotCount].vel.x = 500*cos(3.14*(ship.sprite.angle-90)/180);
        shot[shotCount].vel.y = 500*sin(3.14*(ship.sprite.angle-90)/180);
        
        shotCount = (shotCount+1)%5;
    }
    
    // === Game Render ===
    SDL_RenderClear(renderer);
    
    if (deathTimer > 2 || ((i32)(deathTimer*8))%2 == 0)
        HSE_render_entity(&ship, renderer);
    
    // Update asteroids
    for (int i = 0; i < 39; i++) 
    {
        if (!ast[i].dead) 
        {
            ast[i].pos.x += ast[i].vel.x * delta;
            ast[i].pos.y += ast[i].vel.y * delta;
            
            if (ast[i].pos.y < 0) 
                ast[i].pos.y = WINDOW_HEIGHT - 1;
            else if (ast[i].pos.y > WINDOW_HEIGHT) 
                ast[i].pos.y = 1;
            if (ast[i].pos.x < 0) 
                ast[i].pos.x = WINDOW_WIDTH - 1;
            else if (ast[i].pos.x > WINDOW_WIDTH)
                ast[i].pos.x = 1;
            
            if (checkCircleCollision(ast[i].pos.x, ship.pos.x,  ast[i].pos.y, ship.pos.y, ast[i].sprite.rect.h*0.5) &&deathTimer > 2)
            {
                deathTimer = 0;
                lives--;
                if (lives <= 0)
                {
                    lives = 3;
                    score = 0;
                    scoreMult = 10;
                    printf("dead!\n");
                    resetLevel();
                    astCount = 3;
                }
                
            }
            HSE_render_entity(&ast[i], renderer);
        }
    }
    
    // Update shots
    for (int i = 0; i < 5; i++)
    {
        if (shot[i].dead == false)
        {
            // big asteroids collision detection
            for (int j = 0; j < 3; j++)
            {	
                if (!ast[j].dead && checkCircleCollision(shot[i].pos.x, ast[j].pos.x,
                                                         shot[i].pos.y, ast[j].pos.y, 64))
                {
                    ast[j].dead = true;
                    shot[i].dead = true;
                    
                    i32 d = j*3;
                    
                    ast[3 + d].dead = false;
                    ast[3 + d].pos = ast[j].pos;
                    
                    ast[4 + d].dead = false;
                    ast[4 + d].pos = ast[j].pos;
                    
                    ast[5 + d].dead = false;
                    ast[5 + d].pos = ast[j].pos;
                    astCount += 2;
                    score += scoreMult;
                }
            }
            
            // Medium asteroids collision detection
            for (int j = 3; j < 12; j++)
            {
                if (!ast[j].dead && checkCircleCollision(shot[i].pos.x, ast[j].pos.x,
                                                         shot[i].pos.y, ast[j].pos.y, 32))
                {
                    ast[j].dead = true;
                    
                    shot[i].dead = true;
                    i32 d = (j-3)*3;
                    ast[12 + d].dead = false;
                    ast[12 + d].pos = ast[j].pos;
                    
                    ast[13 + d].dead = false;
                    ast[13 + d].pos = ast[j].pos;
                    
                    ast[14 + d].dead = false;
                    ast[14 + d].pos = ast[j].pos;
                    
                    astCount += 2;
                    score += 2*scoreMult;
                }
            }
            
            // Small asteroids collision detection
            for (int j = 12; j < 39; j++)
            {
                if(!ast[j].dead && checkCircleCollision(shot[i].pos.x, ast[j].pos.x,
                                                        shot[i].pos.y, ast[j].pos.y, 17))
                {
                    ast[j].dead = true;
                    shot[i].dead = true;
                    astCount--;
                    score += 4*scoreMult;
                }
            }
            
            if (astCount <= 0)
            {
                resetLevel();
                scoreMult *= 2;
                astCount = 3;
            }
            
            if (shot[i].pos.y < 0) 
                shot[i].pos.y = WINDOW_HEIGHT - 1;
            else if (shot[i].pos.y > WINDOW_HEIGHT) 
                shot[i].pos.y = 1;
            if (shot[i].pos.x < 0) 
                shot[i].pos.x = WINDOW_WIDTH - 1;
            else if (shot[i].pos.x > WINDOW_WIDTH) 
                shot[i].pos.x = 1;
            
            shot[i].pos.x += shot[i].vel.x * delta;
            shot[i].pos.y += shot[i].vel.y * delta;
            
            HSE_render_entity(&shot[i], renderer);
        }
    }
    
    SDL_Color textColor = {255, 255, 255};
    SDL_Color textColorBG = {0, 0, 0};
    char text[256];
    
    sprintf(text, "LIVES %d     SCORE %d", lives, score);
    
    SDL_Surface *textSurface = TTF_RenderText_Shaded(font, text, textColor, textColorBG);
    SDL_Texture *textTexture = SDL_CreateTextureFromSurface(renderer, textSurface);
    SDL_FreeSurface(textSurface);
    
    i32 tW = 0, tH = 0;
    SDL_QueryTexture(textTexture, NULL, NULL, &tW, &tH);
    
    SDL_Rect textRect = {0, 0, tW, tH};
    SDL_RenderCopy(renderer, textTexture, NULL, &textRect);
    
    SDL_RenderPresent(renderer);
    // === Profiling ===
    
    SDL_DestroyTexture(textTexture);
}

int main(int argc, char ** argv)
{
    init();
    
    bool32 quitb = false;
    SDL_Event event;
    f32 delta = 0;
    
    /* Main game loop */
    while (!quitb) 
    {
        persistent u32 keys = 0x00;
        
        float initTime = SDL_GetTicks();
        
        
        // === Input Handling ===
        SDL_PollEvent (&event);
        switch (event.type)
        {
            case SDL_QUIT:
            {
                quitb = true;
            } break;
            
            case SDL_KEYDOWN:
            {
                if (event.key.keysym.sym == SDLK_RIGHT) 
                    keys = keys | 0x01;
                if (event.key.keysym.sym == SDLK_LEFT ) 
                    keys = keys | 0x02;
                if (event.key.keysym.sym == SDLK_UP   ) 
                    keys = keys | 0x04; 
                if (event.key.keysym.sym == SDLK_z    ) 
                    keys = keys | 0x08; 
                if (event.key.keysym.sym == SDLK_q)
                    quitb = true;
            } break;
            
            case SDL_KEYUP:
            {
                if (event.key.keysym.sym == SDLK_RIGHT) keys = keys & (~0x01);
                if (event.key.keysym.sym == SDLK_LEFT ) keys = keys & (~0x02);
                if (event.key.keysym.sym == SDLK_UP   ) keys = keys & (~0x04); 
                if (event.key.keysym.sym == SDLK_z    ) keys = keys & (~0x08); 
            } break;
        }
        updateAndRenderer(delta, keys);
        delta = (SDL_GetTicks() - initTime)/1000.0;
    }
    
    quit();
    return 0;
}
