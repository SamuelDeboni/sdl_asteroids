// Only include once
#ifndef HYDENGINE_H
#define HYDENGINE_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


typedef int8_t   i8;
typedef uint8_t  u8;
typedef int16_t  i16;
typedef uint16_t u16;
typedef int32_t  i32;
typedef uint32_t u32;
typedef int64_t  i64;
typedef uint64_t u64;
typedef int32_t  bool32;

typedef float  f32;
typedef double f64;

#define internal    static
#define global      static
#define persistent  static

// Global variables
i32 WINDOW_HEIGHT = 720;
i32 WINDOW_WIDTH = 1280;

SDL_Window * window;
SDL_Renderer * renderer;

SDL_Texture* potato_texture;
SDL_Texture* potato_texture2;

TTF_Font *font; 

// Structs
struct vector2
{
    f32 x;
    f32 y;
};

struct HSE_Sprite
{
    SDL_Texture* texture;
    SDL_Rect rect;
    SDL_Rect crop;
    SDL_Point center;
    f32 angle;
    SDL_RendererFlip flip;
};

#endif
